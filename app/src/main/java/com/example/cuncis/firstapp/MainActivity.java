package com.example.cuncis.firstapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText etRupiah, etDollar;
    Button btnConvert;
    RadioButton rbRupiahToDollar, rbDollarToRupiah;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etRupiah = findViewById(R.id.et_rupiah);
        etDollar = findViewById(R.id.et_dollar);
        btnConvert = findViewById(R.id.btn_convert);
        rbRupiahToDollar = findViewById(R.id.rb_rupiah_to_dollar);
        rbDollarToRupiah = findViewById(R.id.rb_dollar_to_rupiah);
        rbDollarToRupiah.setChecked(true);

        btnConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rbRupiahToDollar.isChecked()) {
                    convertRupiahToDollar();
                }
                if (rbDollarToRupiah.isChecked()) {
                    convertDollarToRupiah();
                }
            }
        });
    }

    private void convertRupiahToDollar() {
        try {
            double val = Double.parseDouble(etRupiah.getText().toString());
            etDollar.setText(Double.toString( val / 14000));
        } catch (Exception e) {
            Toast.makeText(this, "Error!\n" +
                    "Please check your RadioButton", Toast.LENGTH_SHORT).show();
            Log.d("Error", "convertRupiahToDollar: " + e.getMessage());
        }

    }

    private void convertDollarToRupiah() {
        try {
            double val = Double.parseDouble(etDollar.getText().toString());
            etRupiah.setText(Double.toString( val * 14000));
        } catch (Exception e) {
            Toast.makeText(this, "Error!\n" +
                    "Please check your RadioButton", Toast.LENGTH_SHORT).show();
            Log.d("Error", "convertDollarToRupiah: " + e.getMessage());
        }

    }
}
